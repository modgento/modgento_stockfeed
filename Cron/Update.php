<?php

namespace Modgento\StockFeed\Cron;

use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\File\Csv;
use Magento\Framework\HTTP\Client\Curl;

class Update
{

    private $endpoint = 'https://www.artisanfurniture.net/livestock.php';

    protected $csv;

    protected $client;

    protected $repository;

    protected $factory;

    public function __construct(
        Csv $csv,
        Curl $client,
        ProductRepository $repository,
        ProductFactory $factory
    ) {
        $this->csv = $csv;
        $this->client = $client;
        $this->repository = $repository;
        $this->factory = $factory;
    }

    public function execute()
    {
        // Get contents from endpoint and split into lines
        $this->client->get($this->endpoint);
        $raw = $this->client->getBody();
        $data = $this->sanitise($raw);

        // $data[0] is supplier_id
        // $data[2] is qty
        foreach ($data as $number => $line) {
            $values = explode(",", $line);
            try {
                $product = $this->factory->create()->loadByAttribute('supplier_id', $values[0]);
                $previousQty = $product->getQty();
                $product->setQty($values[2]);
                $product->save();
            } catch (Exception $e) {
                // want this to continue if fails
            }
        }
    }

    private function sanitise(String $raw)
    {
        $data = explode("\n", $raw);
        unset($data[0]);
        return $data;
    }

}
